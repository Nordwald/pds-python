# Threading and server imports
from socket import gethostbyname, gethostname
from xmlrpclib import ServerProxy
from SimpleXMLRPCServer import SimpleXMLRPCServer
from SocketServer import ThreadingMixIn
from threading import Thread

# util imports
from datetime import datetime
from time import time, sleep
from random import randint, choice, random

# Allows asynchron access to the XMLRPC-Server.
class AsyncXMLRPCServer(ThreadingMixIn,SimpleXMLRPCServer): pass

class Node(object):
	""" Class to implement unsecure base distributed
		calculations. May be extended to add synchronisation features."""

	# The duration of a single calculation session
	DURATION = 20
	# The range to select a port from
	MINPORT  = 1024
	MAXPORT  = 8192

	def __init__(self, hostname=None):
		""" Initalization function. 
			Sets up the class and starts the server.

			Keyword arguments:
			hostname -- a hostname to connect to
		"""
		# Set up the server and its thread
		self.address = gethostbyname('%s.local' % gethostname())
		self.port = randint(Node.MINPORT, Node.MAXPORT)
		self.hostname = 'http://%s:%d' % (self.address, self.port)
		self.server = AsyncXMLRPCServer((self.address, self.port), logRequests=False)
		# Init fields to be used later
		self.value = None
		self.nodes = []
		self.started = False
		self.wantsToken = False
		# Start the server and connect to hostname
		self.thread = Thread(target=self._loop)
		self.thread.start()
		if hostname:
			self._connect(hostname)

	def _log(self, msg):
		""" Intern method to log messages more nicely. 

			Keyword arguments:
			msg -- the message to log
		"""
		stamp = datetime.now().strftime('%H:%M:%S')
		print '[%s] [%s] %s' % (self.hostname[7:], stamp, msg)

	def _loop(self):
		""" The Server main 'loop'. """
		self._log('Started node')
		self.server.register_instance(self)
		# Renaming all functions to be compatible with java-nonsense
		self.server.register_function(self.join, 'Service.join')
		self.server.register_function(self.start, 'Service.start')
		self.server.register_function(self.leave, 'Service.leave')
		self.server.register_function(self.calculate, 'Service.calculate')
		self.server.serve_forever()

	def _connect(self, hostname):
		""" Connects to an existing node network.

			Keyword arguments:
			hostname -- a hostname to call join on
		"""
		hosts = [hostname]
		while hosts:
			host = hosts.pop()
			client = ServerProxy(host)
			self.nodes.append(host)
			# add new nodes to 'hosts' to progress them
			remote = client.Service.join(self.hostname)
			for node in remote:
				if not node in self.nodes and node != self.hostname and node not in hosts:
					hosts.append(node)

	def _disconnect(self):
		""" Disconnects from all connected nodes. """
		while self.nodes:
			node = self.nodes.pop()
			client = ServerProxy(node)
			client.Service.leave(self.hostname)

	def _calculation(self):
		""" Fire random calculations for the set duration. """
		while time() - self.started < Node.DURATION:
			sleep((int)(random()/5*Node.DURATION))
			self.wantsToken = time()

	def _fire(self):
		""" Sends a random calculation to the node network. """
		method = choice(['sum', 'sub', 'div', 'mul'])
		value = randint(1, 20)
		self._log('Fire %s, %d' % (method, value))
		self.calculate(method, value)
		for node in self.nodes:
			try:
				client = ServerProxy(node)
				client.Service.calculate(method, value)
			except Exception, e:
				pass

	def _sendStart(self, initValue):
		""" Starts the distributed calculation session. 

			Keyword arguments:
			initValue -- the value to begin calculations with
		"""
		for node in self.nodes:
			client = ServerProxy(node)
			client.start(initValue)
		self.start(initValue)

	def start(self, initValue):
		""" Starts the calculation session. 

			Keyword arguments:
			initValue -- the value to begin calculations with
		"""
		self._log('Stared calculations with the value "%d"' % initValue)
		self.value = initValue
		self.started = time()
		Thread(target=self._calculation).start()
		return True

	def join(self, hostname):
		""" Joins the network of the current node. 

			Keyword arguments:
			hostname -- the hostname of the new client
		"""
		self._log('%s joined' % hostname)
		self.nodes.append(hostname)
		# "discoery mode"
		return self.nodes

	def leave(self, hostname):
		""" Leaves the nodes network. 

			Keyword arguments:
			hostname -- the hostname of the client to leave
		"""
		if hostname in self.nodes:
			self.nodes.remove(hostname)
			return True
		return False

	def calculate(self, operand, value):
		""" Start a calculation on the node.

			Keyword arguments:
			operand -- either 'sum', 'sub', 'div' or 'mul'
			value -- the value for the operation
		"""
		if operand == 'sum':
			self.value += value
		elif operand == 'sub':
			self.value -= value
		elif operand == 'div':
			self.value /= value
		elif operand == 'mul':
			self.value *= value
		self._log('= %d' % self.value)
		return True