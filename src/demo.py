from sys import argv
from argparse import ArgumentParser

from TokenNode import TokenNode
from SecureNode import SecureNode

from random import randint

class Parser(object):
	def __init__(self):
		self.parser = ArgumentParser(prog='NodeDemo', description='Runs a demo of TokenNode or SecureNode.')
		self.parser.add_argument('type', default='token', type=str, help='The type of node to use.')
		self.parser.add_argument('-s', '--start', action='store_true', help='Start the calculation session after initalization.')
		self.parser.add_argument('-n', '--number', type=int, default=1, help='The number of nodes to set up.')
		self.parser.add_argument('-v', '--value', type=int, default=randint(1, 20), help='The inital value for calculations.')
		self.parser.add_argument('-h', '--hostname', type=str, default=None, help='A host to connect to.')

	def parse(self, args):
		""" Returnes the parsed arguments. """
		return vars(self.parser.parse_args(args))

def main(args=None):
	if not args:
		args = argv[1:]
	arguments = Parser().parse(args)
	if 'token' in arguments['type']:
		nodeclass = TokenNode 
	else:
		nodeclass = SecureNode
	demo(nodeclass, arguments['number'], arguments['value'], 
		arguments['start'], arguments['hostname'])

def demo(nodeclass, n, value, doStart, host):
	""" For testing purposes only. """
	last = None
	nodes = []
	# setup all nodes
	for i in range(n):
		if not hostname:
			node = nodeclass(last)
		else:
			node = nodeclass(hostname)
		nodes.append(node)
		last = node.hostname
	# start all nodes
	if doStart:
		for node in nodes:
			node.start(value)

if __name__ == "__main__":
	main()