from Node import Node

from threading import Thread
from xmlrpclib import ServerProxy

from Queue import PriorityQueue
from time import time, sleep

class SecureNode(Node):
	""" A node using the Ricart & Agrawala algorithm.

		Keyword arguments:
		hostname -- a hostname to connect to
	"""

	def __init__(self, hostname=None):
		super(SecureNode, self).__init__(hostname)
		self.queue 			= PriorityQueue()
		self.accessing 		= False
		self.requested 		= False
		self.acks 			= 0
		self.tokenThread 	= Thread(target=self._handleToken)
		self.tokenThread.start()

	def _sendOK(self, hostname):
		""" Intern method which accepts the request of a node.

			Keyword arguments:
			hostname -- the hostname of the request
		"""
		client = ServerProxy(hostname)
		client.OK()

	def _requestToken(self):
		""" Intern method which requests the 'token'. """
		for node in self.nodes:
			client = ServerProxy(node)
			client.handle(self.hostname, self.wantsToken)

	def _handleToken(self):
		""" Intern loop which triggers calculations. """
		while True:
			sleep(0.20)
			ack = self.acks == len(self.nodes)
			#self._log(self.acks)
			if self.wantsToken and ack:
				# access ressource
				self.accessing = True
				self._fire()
				# Empty the queue for others
				self.acks = 0
				self.accessing = False
				self.wantsToken = False
				self.requested = False
				while not self.queue.empty():
					self._sendOK(self.queue.get()[1])
			# check if we need to request the token
			elif self.wantsToken and not self.requested:
				self._requestToken()
				self.requested = True

	def OK(self):
		""" Method which recieves acknowledgements to requests. """
		self.acks += 1
		return True

	def handle(self, hostname, timestamp):
		""" Method which handles requests. """
		if not self.accessing and not self.wantsToken:
			self._sendOK(hostname)
		# we are accessing the token
		elif self.accessing:
			self.queue.put((timestamp, hostname))
		# we want to access the token outselves
		elif self.wantsToken:
			if float(timestamp) <= float(self.wantsToken):
				self._sendOK(hostname)
			else:
				# Our request is more recent
				self.queue.put((timestamp, hostname))
		return True