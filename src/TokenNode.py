from Node import Node

from xmlrpclib import ServerProxy
from threading import Thread

from time import time, sleep

__author__  = "Niklas Bergmann"
__email__   = "niklas.bergmann@fkie.fraunhofer.de"
__version__ = "1.0"

class TokenNode(Node):
	""" A node using the Token Ring algorithm.

		Keyword arguments:
		hostname -- a hostname to connect to
	"""
	def __init__(self, hostname=None):
		super(TokenNode, self).__init__(hostname)
		self.hasToken 		= False if not hostname else True
		self.tokenVal 		= 0
		self.tokenThread 	= Thread(target=self._handleToken)
		self.server.register_function(self.tokenReceive, 'Service.tokenReceive')
		self.tokenThread.start()

	def _handleToken(self):
		""" Loop which triggers calculations. """
		while True:
			sleep(0.20)
			if self.wantsToken and self.hasToken:
				self._fire()
				self.wantsToken = False
			if self.hasToken:
				self.hasToken = False
				self._shootToken(self.tokenVal + 1)

	def _shootToken(self, token):
		""" Shots the token to the next node.

			Keyword arguments:
			token -- the token value
		"""
		node = self._getNextNode()
		client = ServerProxy(node)
		client.Service.tokenReceive(token)

	def _getNextNode(self):
		""" Returnes the address of the next node
			in line for the token. """
		nodes = [self.hostname]
		nodes.extend(self.nodes)
		nodes.sort()
		index = nodes.index(self.hostname) + 1
		if index == len(nodes):
			index = 0
		return nodes[index]

	def tokenReceive(self, token):
		""" Revieces the token from a node.

			Keyword arguments:
			token -- the token value
		"""
		#self._log('Recieved token %d' % token)
		self.tokenVal = token
		self.hasToken = True
		return True

#node = TokenNode('http://localhost:4544')